# Docker-Blender

Dockerfile for Docker image to render Blender projects. 
Uses Docker volumes to pass project files and receive render output.

## Motivation
- easily render Blender projects on any server
- easily switch Blender versions
- do not mess around with dependencies

## Docker Images
The images are based on Ubuntu. Check the [Project Container Registry](https://gitlab.com/soerenmetje/docker-blender/container_registry)
for all Docker Images. It includes multiple Tags for various Blender versions.

## Requirements
- installed Docker Engine (I used version 20.10.2)

---

## Rendering Examples

You need to:
- locate your project files inside the `project` directory (create it if not exist).
- change blender command to match your .blend file name and your needs. 

After successful execution, the rendered output is located in the `renders` directory.
### Rendering Animation
```shell
docker run --name blender --rm -v "$PWD/project":/app/project -v "$PWD/renders":/app/renders\
  registry.gitlab.com/soerenmetje/docker-blender \
  ./project/file.blend -o /app/renders/frame_##### -s 2500 -e 3000 -a -- --cycles-device CPU
```
### Rendering Single Frame
```shell
docker run --name blender --rm -v "$PWD/project":/app/project -v "$PWD/renders":/app/renders\
  registry.gitlab.com/soerenmetje/docker-blender \
  ./project/file.blend -o /app/renders/frame_##### -f 2500 -- --cycles-device CPU
```

## Help on Blender Commands
For further help and instructions see https://docs.blender.org/manual/en/latest/advanced/command_line/render.html 
or Blender help.
```shell
docker run --name blender --rm registry.gitlab.com/soerenmetje/docker-blender --help
```

---

## Build Image
In case you want to build the image on your own, navigate inside project root directory and execute following command:

```shell
docker build -t docker-blender .
```
If you want to build the image for a specific Blender version you can set build arguments. 
An overview about all available versions can be found on https://download.blender.org/release/.
Specify the URL to the Blender binaries. Use linux 64 builds.
```shell
docker build --build-arg BLENDER_URL=https://download.blender.org/release/Blender2.79/blender-2.79b-linux-glibc219-x86_64.tar.bz2 -t docker-blender:2.79 .
```

## Bonus: Create Video of .png File Set and .mp3 File
You can use ffmpeg to create a video of a set of .png files. Source: https://hamelot.io/visualization/using-ffmpeg-to-convert-a-set-of-images-into-a-video/
```shell
ffmpeg -r 30 -f image2 -s 1920x1080 -i frame_%05d.png -vcodec libx264 -crf 10  -pix_fmt rgb8 video.mp4
```
Start with a specific frame:
```shell
ffmpeg -r 30 -f image2 -s 1920x1080 -start_number 2500 -i frame_%05d.png -vcodec libx264 -crf 10  -pix_fmt rgb8 video.mp4
```
- `-r` sets the framerate (fps)
- `-crf` sets the quality, lower means better quality, 15-25 is usually good
- `-s` sets the resolution
- `-pix_fmt` specifies the pixel format
- `-start_number` sets the number of the first frame


Display all pixel formats:
```shell
ffmpeg -pix_fmts
```

Add audio to video file:
```shell
ffmpeg -i video.mp4 -i audio.mp3 -c copy -map 0:v:0 -map 1:a:0 video_audio.mp4 
```
